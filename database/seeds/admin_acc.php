<?php

use Illuminate\Database\Seeder;

class admin_acc extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'lastname' => 'admin',
            'birthdate' => '1999/09/09',
            'mobilenumber' => '5555555',
            'type' => '1',
            'email' => 'admin@idrive.ge',
            'id_number' => '11111111111',
            'password' => bcrypt('idrive1415'),
        ]);
    }
}
