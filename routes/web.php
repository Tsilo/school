<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/teacher', 'TeacherController@lesson');
Route::get('/student', function () {
    return view('student');
});


#///# Static Pages
Route::get('/about', function () {
    return view('static.about');
});
Route::get('/contact', function () {
    return view('static.contact');
});
Route::get('/faq', function () {
    return view('static.faq');
});
Route::get('/terms', function () {
    return view('static.terms');
});


Route::post('/ajaxlogin', 'UserController@login');

#///# Administrator routes
Route::group(['prefix' => 'admin', 'middleware' => 'role:admin'], function () {
    Route::get('/', 'AdminController@index');
    Route::resource('/user', 'UserController');
    Route::resource('/course', 'CourseController');
    Route::resource('/lesson', 'LessonController');
});


Route::group(['middleware' => 'auth'], function () {
    #///# User routes
    Route::get('/dashboard', 'UserController@userDashboard');
    Route::get('/schedule', 'UserController@userLessons');
    Route::get('/payment', 'UserController@payment');
    #///# Calendar Routes
    Route::get('/calendar', 'CalendarController@index');
    Route::post('/calendar/nextTwoDays', 'CalendarController@nextTwoDaysData');
    Route::post('/calendar/store', 'CalendarController@saveUserLessons');


    #///# teacher routes
    Route::group(['prefix' => 'teacher', 'middleware' => 'role:teacher'], function () {
        Route::get('/', 'TeacherController@index');
        Route::get('/start/{id}', 'TeacherController@startLesson');
    });


    #///# twilio video rooms
    Route::get('/lessons', "TwilioController@showRooms");
    Route::prefix('lesson')->group(function () {
        Route::post('join', 'TwilioController@joinRoom');
        Route::post('create', 'TwilioController@createRoom');
        Route::post('check', 'TwilioController@checkRoom');

        Route::get('join/{id}', 'UserController@joinLesson');
    });

    #///#other
    Route::post('/lesson/status/update', 'LessonController@changeStatus');
});