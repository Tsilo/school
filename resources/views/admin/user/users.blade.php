@extends('layouts.admin')

@section('content')

    <div class="col-md-8 col-md-offset-2">
        <a class="btn btn-success" href="{{route('user.create')}}">Create User</a>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>სახელი</th>
                <th>გვარი</th>
                <th>E-mail</th>
                <th>ტელ. ნომერი</th>
                <th>ტიპი</th>
                <th><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></th>
                <th><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></th>
            </tr>
            </thead>
            <tbody>
            @foreach ($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->lastname }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->mobilenumber }}</td>
                    <td>@php
                            switch ($user->type){
                                case 3:
                                    echo 'Student';
                                    break;
                                case 2:
                                    echo 'Teacher';
                                    break;
                                case 1:
                                    echo 'Admin';
                                    break;
                            }
                        @endphp
                    </td>
                    <td><a type="button" href="{{route('user.edit', $user->id)}}" class="btn btn-info btn-xs"><span
                                    class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
                    <td>
                        <button type="button" class="btn btn-danger btn-xs deleteRow"
                                data-url="{{route('user.destroy', $user->id)}}">
                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <div class="text-center">
            {{ $users->links() }}
        </div>
    </div>

@endsection

@section('scripts')
@endsection