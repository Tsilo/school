@extends('layouts.admin')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <form class="form-horizontal" role="form" method="POST" action="{{ route('user.store') }}">
            {{ csrf_field() }}
            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="firstname" class="form-label">საელი</label>
                <input id="firstname" type="text" class="form-control" name="name"
                       value="{{ old('name') }}" required autofocus>
                @if ($errors->has('name'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                <label for="lastname" class="form-label">გვარი</label>
                <input id="lastname" type="text" class="form-control" name="lastname"
                       value="{{ old('lastname') }}" required>
                @if ($errors->has('lastname'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('id_number') ? ' has-error' : '' }}">
                <label for="id_number" class="form-label">პირადი ნომერი</label>
                <input id="id_number" type="text" class="form-control" name="id_number"
                       value="{{ old('id_number') }}" required>
                @if ($errors->has('id_number'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('id_number') }}</strong>
                                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="form-label">Email</label>
                <input id="email" type="text" class="form-control" name="email"
                       value="{{ old('email') }}" required>
                @if ($errors->has('email'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="form-label">პაროლი</label>
                <input id="password" type="password" class="form-control" name="password" required>
                @if ($errors->has('password'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                @endif
            </div>
            <div class="form-group @if ($errors->has('birthday') || $errors->has('birthmonth') || $errors->has('birthyear')) has-error @endif">
                <div class="date-note">რეგისტრაციისთვის საჭიროა იყოთ 16 წელს ზევით</div>
                <div class="form-select">
                    <select class="day" name="birthday">
                        <option value="0" disabled selected>დღე</option>
                        @for ($i = 1; $i < 32; $i++)
                            <option value="{{ $i }}" {{ (old('birthday') == $i ? "selected":"") }}>{{ $i }}</option>
                        @endfor
                    </select>
                    <select class="month" name="birthmonth">
                        <option value="0" disabled selected>თვე</option>
                        @php
                            $months =  config('global.months');
                        @endphp
                        @for ($i = 1; $i < 13; $i++)
                            <option value="{{ $i }}" {{ (old('birthmonth') == $i ? "selected":"") }}>
                                {{ $months[$i] }}</option>
                        @endfor
                    </select>
                    <select class="year" name="birthyear">
                        <option value="0" disabled selected>წელი</option>
                        <?php $year = date("Y") - 16; ?>
                        @for ($i = $year; $i > $year - 51; $i--)
                            <option value="{{ $i }}" {{ (old('birthyear') == $i ? "selected":"") }}>{{ $i }}</option>
                        @endfor
                    </select>
                </div>
                @if ($errors->has('birthday') || $errors->has('birthmonth') || $errors->has('birthyear'))
                    <span class="help-block">
                                        <strong>აირჩიეთ დაბადების თარიღი</strong>
                                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('mobilenumber') ? ' has-error' : '' }}">
                <label for="mobile-number-reg" class="form-label">მობილური ტელეფონი</label>
                <input id="mobile-number-reg" type="text" class="form-control" name="mobilenumber"
                       value="{{ old('mobilenumber') }}" required>
                @if ($errors->has('mobilenumber'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('mobilenumber') }}</strong>
                                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                <label for="user-type" class="form-label">მომხმარებლის ტიპი</label>
                <select id="user-type" type="text" class="form-control" name="type">
                    <option value="3" {{ (old('type') == 3 ? "selected":"") }}>user</option>
                    <option value="2" {{ (old('type') == 2 ? "selected":"") }}>teacher</option>
                    <option value="1" {{ (old('type') == 1 ? "selected":"") }}>admin</option>
                </select>
                @if ($errors->has('type'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                @endif
            </div>

            <div class="form-group">
                <button class="btn btn-success pull-right" type="submit">შენახვა</button>
            </div>
        </form>
    </div>

@endsection

@section('scripts')
@endsection