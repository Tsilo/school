@extends('layouts.admin')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <form class="form-horizontal" role="form" method="POST" action="{{ route('course.update', $course->id) }}">
            {{ csrf_field() }}
            {{ method_field('put') }}
            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name" class="form-label">საელი</label>
                <input id="name" type="text" class="form-control" name="name"
                       value="{{ old('name', $course->name) }}" required autofocus>

                @if ($errors->has('name'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('teacher_id') ? ' has-error' : '' }}">
                <label>მთავარი მასწავლებელი</label>
                <select name="teacher_id" class="form-control">
                    @foreach($teachers as $teacher)
                        <option value="{{$teacher->id}}"
                                {{ old('teacher_id', $course->teacher_id) == $teacher->id ? 'selected' : '' }}
                        >{{$teacher->name}}</option>
                    @endforeach
                </select>
                @if ($errors->has('teacher_id'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('start_date') ? ' has-error' : '' }}">
                @php
                    $carbon = new \Carbon\Carbon($course->start_date);
                    $dbdate = $carbon->format('d/m/y');
                    $hour = $carbon->format('H:i');
                @endphp
                <label for="date">დაწყების დრო</label>
                <div class="row">
                    <div class="col-md-8">
                        <div class="input-group date datepicker ">

                            <input type="text" id="date" name="start_date" class=" form-control"
                                   value="{{$dbdate}}"><span
                                    class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <select name="start_hour" class="form-control">
                            @for($i = 1; $i <= 24; $i++)
                                @php
                                    $date = new \Carbon\Carbon("18:00");
                                @endphp
                                <option value="{{$date->format('H:i')}}">{{$date->format('H:i')}}</option>
                            @endfor
                        </select>
                    </div>
                    @if ($errors->has('start_date'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <button class="btn btn-success pull-right" type="submit">კურსის შენახვა</button>
            </div>
        </form>
    </div>
<div class="clearfix"></div>
    <h3>გაკვეთილების სია</h3>
    <form class="form-horizontal" role="form" method="POST" action="{{ route('lesson.update', 0) }}">
        {{ csrf_field() }}
        {{ method_field('put') }}
        @foreach($course->lessons as $k => $lesson)
            <div class="row">
                <div class="col-md-8">
                    <div class="{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="form-label">{{$k+1}} გაკვეთილის სახელი</label>
                        <input type="hidden" name="id[]" value="{{$lesson->id}}">
                        <input id="name" type="text" class="form-control" name="name[]"
                               value="{{ old('name', $lesson->name) }}" required autofocus>

                        @if ($errors->has('name'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-4 ">
                    <div class="{{ $errors->has('teacher_id') ? ' has-error' : '' }}">
                        <label>მასწავლებელი</label>
                        <select name="teacher_id[]" class="form-control">
                            @foreach($teachers as $teacher)
                                <option value="{{$teacher->id}}"
                                        {{ $lesson->teacher_id == $teacher->id ? 'selected' : '' }}
                                >{{$teacher->name}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('teacher_id'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="{{ $errors->has('start_date') ? ' has-error' : '' }}">
                        @php
                            $carbon = new \Carbon\Carbon($lesson->start_date);
                            $dbdate = $carbon->format('d/m/y');
                            $hour = $carbon->format('H:i');
                        @endphp
                        <label for="date">დაწყების დრო</label>

                        <div class="input-group date datepicker ">
                            <input type="text" id="date" value="{{$dbdate}}" name="start_date[]"
                                   class=" form-control"><span
                                    class="input-group-addon"><i
                                        class="glyphicon glyphicon-th"></i></span>
                        </div>
                        @if ($errors->has('start_date'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="{{ $errors->has('start_hour') ? ' has-error' : '' }}">
                        <label>საათი</label>
                        <select name="start_hour[]" class="form-control">
                            @for($i = 1; $i <= 24; $i++)
                                @php
                                    $date = new \Carbon\Carbon("$i:00");
                                @endphp
                                <option value="{{$date->format('H:i')}}"
                                        {{ $hour == $date->format('H:i') ? 'selected' : '' }}
                                >{{$date->format('H:i')}}</option>
                            @endfor
                        </select>
                    </div>
                </div>
            </div>
            <hr/>

        @endforeach

        <div class="form-row">
            <button class="btn btn-success pull-right" type="submit">გაკვეთილების შენახვა</button>
        </div>
        <br/>
    </form>
    </div>


@endsection

@section('scripts')
@endsection