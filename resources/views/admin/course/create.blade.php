@extends('layouts.admin')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <form class="form-horizontal" role="form" method="POST" action="{{ route('course.store') }}">
            {{ csrf_field() }}
            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name" class="form-label">საელი</label>
                <input id="name" type="text" class="form-control" name="name"
                       value="{{ old('name') }}" required autofocus>

                @if ($errors->has('name'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('teacher_id') ? ' has-error' : '' }}">
                <label>მთავარი მასწავლებელი</label>
                <select name="teacher_id" class="form-control">
                    @foreach($teachers as $teacher)
                        <option value="{{$teacher->id}}">{{$teacher->name}}</option>
                    @endforeach
                </select>
                @if ($errors->has('teacher_id'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('start_date') ? ' has-error' : '' }}">

                <label for="date">დაწყების დრო</label>
                <div class="row">
                    <div class="col-md-8">
                        <div class="input-group date datepicker ">

                            <input type="text" id="date" name="start_date" class=" form-control"><span
                                    class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <select name="start_hour" class="form-control">
                            @for($i = 1; $i <= 24; $i++)
                                @php
                                    $date = new \Carbon\Carbon("$i:00");
                                @endphp
                                <option value="{{$date->format('H:i')}}">{{$date->format('H:i')}}</option>
                            @endfor
                        </select>
                    </div>
                    @if ($errors->has('start_date'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <button class="btn btn-success pull-right" type="submit">შენახვა</button>
            </div>
        </form>
    </div>

@endsection

@section('scripts')
@endsection