@extends('layouts.admin')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <a class="btn btn-success" href="{{route('course.create')}}">Create Course</a>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>დასახელება</th>
                <th><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></th>
                <th><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></th>
            </tr>
            </thead>
            <tbody>
            @foreach ($courses as $course)
                <tr>
                    <td>{{ $course->id }}</td>
                    <td>{{ $course->name }}</td>
                    <td><a type="button" href="{{route('course.edit', $course->id)}}"
                           class="btn btn-info btn-xs">
                            <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                        </a></td>
                    <td>
                        <button type="button" class="btn btn-danger btn-xs deleteRow"
                                data-url="{{route('course.destroy', $course->id)}}"
                        ><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <div class="text-center">
            {{ $courses->links() }}
        </div>
    </div>
@endsection

@section('scripts')
@endsection