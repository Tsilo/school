@extends('layouts.admin')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <h3>გაკვეთილების შექმნა</h3>
        <form class="form-horizontal" role="form" method="POST" action="{{ route('lesson.store') }}">
            {{ csrf_field() }}

            <input type="hidden" name="course_id" value="{{$request['course_id']}}">
            @for($g = 1; $g < 11; $g++)
                <div class="row">
                    <div class="col-md-8">
                        <div class="{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="form-label">{{$g}} გაკვეთილის სახელი</label>
                            <input id="name" type="text" class="form-control" name="name[]"
                                   value="{{ old('name') ? old('name') : $lessonNames[$g] }}" required>

                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4 ">
                        <div class="{{ $errors->has('teacher_id') ? ' has-error' : '' }}">
                            <label>მასწავლებელი</label>
                            <select name="teacher_id[]" class="form-control">
                                @foreach($teachers as $teacher)
                                    <option value="{{$teacher->id}}"
                                            {{ $request['teacher_id'] == $teacher->id ? 'selected' : '' }}
                                    >{{$teacher->name}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('teacher_id'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="{{ $errors->has('start_date') ? ' has-error' : '' }}">

                            <label for="date">დაწყების დრო</label>

                            <div class="input-group date datepicker ">
                                <input type="text" id="date" value="{{$dates[$g - 1]}}" name="start_date[]"
                                       class=" form-control"><span
                                        class="input-group-addon"><i
                                            class="glyphicon glyphicon-th"></i></span>
                            </div>
                            @if ($errors->has('start_date'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="{{ $errors->has('start_hour') ? ' has-error' : '' }}">
                            <label>საათი</label>
                            <select name="start_hour[]" class="form-control">
                                @for($i = 1; $i <= 24; $i++)
                                    @php
                                        $date = new \Carbon\Carbon("$i:00");
                                    @endphp
                                    <option value="{{$date->format('H:i')}}"
                                            {{ $request['start_hour'] == $date->format('H:i') ? 'selected' : '' }}
                                    >{{$date->format('H:i')}}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                </div>
                <hr/>

            @endfor

            <div class="form-row">
                <button class="btn btn-success pull-right" type="submit">შენახვა</button>
            </div>
            <br/>
        </form>
    </div>

@endsection

@section('scripts')
@endsection