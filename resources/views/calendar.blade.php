@extends('layouts.app')

@section('content')

    <!-- Content Start -->
    <section class="page-wrap">

        <div class="overlay">
            <div class="notification">
                <div class="header">
                    <div class="icon-success"></div>
                    <div class="text">თქვენ წარმატებით დარეგისტრირდით</div>
                </div>
                <div class="content">
                    <p>კალენდარზე ხელმისაწვდომი ლექციის თარიღები აღნიშნულია მწვანედ <span class="circle"></span> აირჩიეთ
                        სასურველი დღე და საათი და შეადგინეთ განრიგი.</p>
                </div>
                <div class="center">
                    <div class="button ok-btn">კარგი</div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="steps">
                <ul class="step-list">
                    <li class="complete">
                        <div class="number"></div>
                        <div class="text">რეგისტრაცია</div>
                    </li>
                    <li class="active">
                        <div class="number"></div>
                        <div class="text">ცხრილი</div>
                    </li>
                    <li>
                        <div class="number"></div>
                        <div class="text">გადახდა</div>
                    </li>
                </ul>
            </div>
            @if(empty($select))
                <div class="text-center">ამ დროისთვის გაკვეთილები არ არის</div>
            @else
                @foreach($calDays as $m=>$calMonth)
                    <div class="calendar {{ $m == 0 ? '' : 'hidden' }}" id="calendarMonth{{$m}}">
                        <div class="month-switcher clear-fix">
                            <div class="left-arrow  {{ $m == 0 ? 'disabled-arrow' : '' }}"><i class="icon-arrow-down"></i></div>
                            <div class="month">{{$month[$m]}}</div>
                            <div class="right-arrow {{ $m == 4 ? 'disabled-arrow' : '' }}"><i class="icon-arrow-down"></i></div>
                        </div>
                        <div class="calendar-container ">
                            <div class="calendar-head">
                                <div class="cal-row">
                                    <div class="cal-col">ორშ</div>
                                    <div class="cal-col">სამ</div>
                                    <div class="cal-col">ოთხ</div>
                                    <div class="cal-col">ხუთ</div>
                                    <div class="cal-col">პარ</div>
                                    <div class="cal-col">შაბ</div>
                                    <div class="cal-col">კვ</div>
                                </div class="cal-row">
                            </div>
                            <div class="calendar-body">
                            @foreach($calMonth as $k=>$day)
                                @if(($k+1) % 7 == 1)
                                    <div class="cal-row">
                                        @endif

                                        <div class="cal-col {{isset($select[$day['val']]) ? 'available' : 'disabled'}} {{$day['val'] == $today ? 'today' : ''}}"
                                            data-date="{{$day['val']}}">{!! $day['disp'] !!}
                                            <div class="days-tooltip ">
                                                <div class="title">ლექციის დრო</div>
                                                <div class="form-select">
                                                    <select>
                                                        @if(isset($select[$day['val']]))
                                                            @foreach($select[$day['val']] as $hour)
                                                                <option value="{{$hour['val']}}">{{$hour['disp']}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                                <div class="button save">შენახვა</div>
                                            </div>

                                            <div class="lesson hidden">
                                                <div class="number"><span>1</span> გაკვეთილი</div>
                                                <div class="time"></div>
                                            </div>
                                        </div>
                                        @if(($k+1) % 7 == 0)
                                    </div>
                                @endif
                            @endforeach

                            </div>
                        </div>

                    </div>
                @endforeach
                    <form method="post" action="{{url('/calendar/store')}}">
                        {{ csrf_field() }}
                        <input type="hidden" name="lesson[]" id="lesson1">
                        <input type="hidden" name="lesson[]" id="lesson2">
                        <input type="hidden" name="lesson[]" id="lesson3">
                        <input type="hidden" name="lesson[]" id="lesson4">
                        <input type="hidden" name="lesson[]" id="lesson5">
                        <input type="hidden" name="lesson[]" id="lesson6">
                        <input type="hidden" name="lesson[]" id="lesson7">
                        <input type="hidden" name="lesson[]" id="lesson8">
                        <input type="hidden" name="lesson[]" id="lesson9">
                        <input type="hidden" name="lesson[]" id="lesson10">
                        <div class="form-row hidden" id='calendarSubmit'>
                            <button  class="form-btn">გაკვეთილების არჩევა</button>
                        </div>
                    </form>
            @endif
        </div>
    </section>
    <!-- Content End -->



@endsection
