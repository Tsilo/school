@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <button class="btn btn-default" autocomplete="off" id="start">Start</button>
                <div class="panel panel-default">
                    <div class="panel-body relative" id="videoremote1"></div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/janus.js') }}"></script>
    <script src="{{ asset('js/student.js') }}"></script>
@endsection