<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'eDrive.ge') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
<header class="@if(Request::is('/')) landing-header  @endif ">
    <div class="container">
        <div class="clear-fix row">
            <div class="col-md-6">
                <a href="{{ url('/') }}" class="logo"></a>
            </div>
            <div class="col-md-6 clear-fix">
                <div class="user-area f-right ">
                    @if (Auth::guest())
                        <a href="javascript:void(0)" id="login-btn"
                           class="login-btn f-left button small white">შესვლა</a>
                        <a href="{{ route('register') }}" class="register-btn f-left button small">რეგისტრაცია</a>
                    @else
                        <a href="#" class="dropdown-toggle login-btn f-left button small white"
                           data-toggle="dropdown" role="button"
                           aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li>
                                @if(Auth::user()->type == 3)
                                    <a href="{{url('/dashboard')}}">დეშბორდი</a>
                                @elseif(Auth::user()->type == 2)
                                    <a href="{{url('/teacher')}}">დეშბორდი</a>
                                @endif
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    გასვლა
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    @endif
                </div>
            </div>
        </div>
    </div>
</header>


@yield('content')

<!-- Footer Start -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="copyright">© iDrive.ge 2018</div>
            </div>
            <div class="col-md-6">
                <ul class="navigation">
                    <li><a href="./">მთავარი</a></li>
                    <li><a href="about">ჩვენს შესახებ</a></li>
                    <li><a href="faq">კითხვა პასუხი</a></li>
                    <li><a href="contact">კონტაქტი</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <div class="social">
                    <a href="#" class="icon-facebook"></a>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Footer End -->


<!-- Popup Start-->
<div class="popup hidden">
    <div class="box">
        <div class="close-box"></div>
        <div class="login">
            <div class="title">საიტზე შესვლა</div>
            <form action="">
                <div class="form-row">
                    <input id="mobile-number" type="text" class="form-input">
                    <label for="mobile-number" class="form-label">მობილური ტელეფონი</label>
                    <span class="help-block hidden">
                                        <strong>ასეთი მომხმარებელი/პაროლი ვერ მოიძებნა</strong>
                                    </span>
                </div>
                <div class="form-row clear-fix">
                    <input id="login-password" type="password" class="form-input">
                    <label for="login-password" class="form-label">პაროლი</label>
                    <a href="#" class="forgot-pass f-right">დაგავიწყდა პაროლი?</a>
                </div>
                <div class="form-row">
                    <button class="form-btn">შესვლა</button>
                </div>
            </form>
            <a href="{{ route('register') }}" class="then-register">არ ხარ რეგისტრირებული?</a>
        </div>

        <div class="recovery hidden">
            <div class="title">პაროლის აღდგენა</div>
            <form action="">
                <div class="form-row">
                    <input id="recovery-mobile-number" type="text" class="form-input">
                    <label for="recovery-mobile-number" class="form-label">მობილური ტელეფონი</label>
                </div>
                <div class="form-row clear-fix">
                    <input id="sms-code" type="text" class="form-input with-btn f-left">
                    <label for="sms-code" class="form-label">SMS კოდი</label>
                    <button class="form-btn small f-right disabled">SMS</button>
                </div>
                <div class="form-row">
                    <input id="recovery-pass" type="password" class="form-input">
                    <label for="recovery-pass" class="form-label">ახალი პაროლი</label>
                </div>
                <div class="form-row">
                    <button class="form-btn">შესვლა</button>
                </div>
            </form>
            <a href="{{ route('register') }}" class="then-register">არ ხარ რეგისტრირებული?</a>
        </div>
    </div>
</div>
<!-- Popup End-->
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>

@yield('scripts')
</body>
</html>