@extends('layouts.app')

@section('content')

    <section class="page-wrap">
        <div class="container">
            <table class="lessons-table">
                <thead>
                <tr>
                    <td></td>
                    <td>ლექცია</td>
                    <td>დაწყების დრო</td>
                    <td>ლექციის თემები</td>
                </tr>
                </thead>
                <tbody>
                @php
                    $months =  config('global.months');
                    $current = true;
                @endphp
                @foreach($schedule as $k => $row)
                    @php
                        $dt = new \Carbon\Carbon($row->start_date);
                    @endphp
                    @if($current && $today < $dt)
                        @php
                        $current = false;
                        @endphp
                        <tr class="current">
                    @else
                        <tr>
                            @endif
                            <td class="status">
                                @if($today < $dt)
                                    <span></span>
                                @elseif($row->pivot->status == 2)
                                    <span class="attend"></span></td>
                            @else
                                <span class="not-attend"></span></td>
                            @endif
                            <td class="lesson">
                                <div class="lesson-number">ლექცია {{$k + 1}}</div>
                                <div class="teacher">{{$row->teacher->name}} {{$row->teacher->lastname}}</div>
                            </td>
                            <td class="date">
                                <span class="lesson-date">{{$dt->day}} {{$months[$dt->month]}}</span>
                                <span class="lesson-time">{{$dt->hour}}:{{$dt->minute}} სთ</span>
                            </td>
                            <td class="lesson-theme">{{$row['name']}}</td>
                        </tr>
                        @endforeach

                </tbody>
            </table>
        </div>
    </section>
@endsection
