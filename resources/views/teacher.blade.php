@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div id="media-div">
                </div>
                {{--<button class="btn btn-default" autocomplete="off" id="start">Start</button>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Local Video <span class="label label-primary hide"
                                                                  id="publisher"></span></h3>
                    </div>
                    <div class="panel-body" id="videolocal"></div>
                </div>--}}
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    {{--<script src="{{ asset('js/janus.js') }}"></script>--}}
    {{-- <script src="{{ asset('js/teacher.js') }}"></script>--}}
    {{--<script src="//media.twiliocdn.com/sdk/js/video/v1/twilio-video.min.js"></script>--}}
    <script>
        $(document).ready(function () {

console.log(Twilio);
            Twilio.createLocalTracks({
                audio: true,
                video: {width: 300}
            }).then(function (localTracks) {
                return Twilio.connect('{{ $accessToken }}', {
                    name: '{{$roomName}}',
                    tracks: localTracks,
                    video: {width: 300}
                });
            }).then(function (room) {
                console.log('Successfully joined a Room: ', room.name);

                room.participants.forEach(participantConnected);

                var previewContainer = document.getElementById(room.localParticipant.sid);
                if (!previewContainer || !previewContainer.querySelector('video')) {
                    participantConnected(room.localParticipant);
                }

                room.on('participantConnected', function (participant) {
                    console.log("Joining: '" + participant.identity + "'");
                    participantConnected(participant);
                });

                room.on('participantDisconnected', function (participant) {
                    console.log("Disconnected: '" + participant.identity + "'");
                    participantDisconnected(participant);
                });
            });

            function participantConnected(participant) {
                console.log('Participant "%s" connected', participant.identity);

                const div = document.createElement('div');
                div.id = participant.sid;
                div.setAttribute("style", "float: left; margin: 10px;");
                div.innerHTML = "<div style='clear:both'>" + participant.identity + "</div>";

                participant.tracks.forEach(function (track) {
                    trackAdded(div, track)
                });

                participant.on('trackAdded', function (track) {
                    trackAdded(div, track)
                });
                participant.on('trackRemoved', trackRemoved);

                document.getElementById('media-div').appendChild(div);
            }

            function participantDisconnected(participant) {
                console.log('Participant "%s" disconnected', participant.identity);

                participant.tracks.forEach(trackRemoved);
                document.getElementById(participant.sid).remove();
            }

            function participantConnected(participant) {
                console.log('Participant "%s" connected', participant.identity);

                const div = document.createElement('div');
                div.id = participant.sid;
                div.setAttribute("style", "float: left; margin: 10px;");
                div.innerHTML = "<div style='clear:both'>" + participant.identity + "</div>";

                participant.tracks.forEach(function (track) {
                    trackAdded(div, track)
                });

                participant.on('trackAdded', function (track) {
                    trackAdded(div, track)
                });
                participant.on('trackRemoved', trackRemoved);

                document.getElementById('media-div').appendChild(div);
            }

            function participantDisconnected(participant) {
                console.log('Participant "%s" disconnected', participant.identity);

                participant.tracks.forEach(trackRemoved);
                document.getElementById(participant.sid).remove();
            }

            function trackAdded(div, track) {
                div.appendChild(track.attach());
                var video = div.getElementsByTagName("video")[0];
                if (video) {
                    video.setAttribute("style", "max-width:300px;");
                }
            }

            function trackRemoved(track) {
                track.detach().forEach(function (element) {
                    element.remove()
                });
            }
        });
    </script>
@endsection