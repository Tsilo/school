@extends('layouts.app')

@section('content')

    <!-- Content Start -->
    <section class="page-wrap">

        <div class="container">
        <table class="lessons-table">
            <thead>
                <tr>
                    <td></td>
                    <td>ლექცია</td>
                    <td>დაწყების დრო</td>
                    <td>ლექციის თემები</td>
                </tr>
            </thead>
            <tbody>
            @foreach($lessons as $k => $lesson)
                <tr>
                    <td class="status">
                        @php
                            switch ($lesson->pivot->status){
                                case 2:
                                    echo '<span class="attend"></span>';
                                    break;
                                case 1:
                                    echo '<span class="not-attend"></span>';
                                    break;
                                default:
                                    echo '<span></span>';
                                    break;
                            }
                        @endphp
                    </td>
                    <td class="lesson">
                        <div class="lesson-number">ლექცია {{$k+1}}</div>
                        <div class="teacher">{{$lesson->teacher->name.$lesson->teacher->lastname}}</div>
                    </td>
                    <td class="date">
                        <span class="lesson-date">{{$lesson->start_date->format('d/m/y')}} </span>
                        <span class="lesson-time">{{$lesson->start_date->format('h:i')}} სთ</span>
                    </td>
                    <td class="lesson-theme">{{$lesson->name}}</td>
                </tr>
            @endforeach

               {{-- <tr class="current">
                    <td class="status"><span></span></td>
                    <td class="lesson">
                        <div class="lesson-number">ლექცია 4</div>
                        <div class="teacher">ცისანა ჭყადუა</div>
                    </td>
                    <td class="date">
                        <span class="lesson-date">20 აპრილი</span>
                        <span class="lesson-time">20:00 სთ</span>
                    </td>
                    <td class="lesson-theme">უწესივრობა და მართვის პირობები</td>
                </tr>
                --}}
            </tbody>
        </table>
        </div>
    </section>
    <!-- Content End -->



@endsection
