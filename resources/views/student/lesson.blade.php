@extends('layouts.app')

@section('content')
    <div class="video-chat">
        <div class="left-side">
            <div class="video-header u-clearfix">
                <a href="https://edrive.ge" class="logo u-float--left"></a>
                <div class="u-float--right u-clearfix">
                    <button class="fullscreen-btn"><i class="icon-fullscreen"></i> ეკრანზე გაშლა</button>
                </div>
            </div>
            <div class="video-content">
                <div id="media-div"></div>
            </div>
            <div class="video-footer u-clearfix">
                <div class="u-float--left u-clearfix">
                    <button class="button-tall u-float--left"><i class="icon-question"></i> კითხვის მოთხოვნა.</button>
                    <button class="button-tall u-float--left"><i class="icon-mute"></i> უხმო</button>
                </div>
                <div class="u-float--right">
                    <ul class="user-list">
                    </ul>
                </div>
            </div>
        </div>
        <div class="right-side">
            <div class="chat-section">
                <div class="chat-content">

                </div>
                <div class="video-footer">
                    <input type="text" class="chat-input" placeholder="აკრიფეთ ტექსტი..">
                    <button class="button-send"><i class="icon-send"></i></button>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('scripts')
    {{-- <script src="{{ asset('js/teacher.js') }}"></script>--}}
    <script>
        $(document).ready(function () {
            var lessonName = '{{$lessonName}}',
                username = '',
                accessToken = '';

            function checkRoomInit() {
                axios.post('/lesson/check', {
                    roomName: lessonName
                }).then(function (response) {
                    console.log(response);
                    if (!response.data.status) {
                        setTimeout(function () {
                            checkRoomInit();
                        }, 1000)
                    }else{
                        getToken();
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            }
            checkRoomInit();



            function getToken() {
                axios.post('/lesson/join', {
                    roomName: lessonName
                })
                    .then(function (response) {
                        accessToken = response.data.accessToken;
                        username = response.data.identity;
                        startChat();
                        joinRoom();
                    })
                    .catch(function (error) {
                        console.log(error);
                    });

            }

            function joinRoom() {
                Twilio.Video.createLocalTracks({
                    audio: true,
                    video: false
                }).then(function (localTracks) {
                    return Twilio.Video.connect(accessToken, {
                        name: lessonName,
                        tracks: localTracks,
                        video: {width: 300}
                    });
                }).then(function (room) {
                    console.log('Successfully joined a Room: ', room.name);

                    room.participants.forEach(participantConnected);

                    var previewContainer = document.getElementById(room.localParticipant.sid);
                    if (!previewContainer || !previewContainer.querySelector('video')) {
                        participantConnected(room.localParticipant);
                    }

                    room.on('participantConnected', function (participant) {
                        console.log("Joining: '" + participant.identity + "'");
                        participantConnected(participant);
                    });

                    room.on('participantDisconnected', function (participant) {
                        console.log("Disconnected: '" + participant.identity + "'");
                        participantDisconnected(participant);
                    });
                });
            }

            function participantConnected(participant) {
                console.log('Participant "%s" connected', participant.identity);
                console.log(participant);
                const div = document.createElement('li');
                div.className = participant.sid;
                div.innerHTML = displayName(participant.identity);

                participant.tracks.forEach(function (track) {
                    trackAdded(div, track, participant.sid)
                });

                participant.on('trackAdded', function (track) {
                    trackAdded(div, track, participant.sid)
                });
                participant.on('trackRemoved', trackRemoved);

                $('.user-list').append(div);
            }


            function participantDisconnected(participant) {
                console.log('Participant "%s" disconnected', participant.identity);

                participant.tracks.forEach(trackRemoved);
                $('.' + participant.sid).remove();
            }

            function trackAdded(div, track, sid) {
                if (track.kind == 'video') {
                    var video = track.attach();
                    video.className = sid
                    $('#media-div').append(video)
                } else {
                    div.appendChild(track.attach());
                }
                /*var video = div.getElementsByTagName("video")[0];
                if (video) {
                    // video.setAttribute("style", "max-width:300px;");
                }*/
            }

            function trackRemoved(track) {
                track.detach().forEach(function (element) {
                    element.remove()
                });
            }


            // Get handle to the chat div
            var $chatWindow = $('#messages');

            // Our interface to the Chat service
            var chatClient;

            // A handle to the "general" chat channel - the one and only channel we
            // will have in this sample app
            var generalChannel;

            // The server will assign the client a random username - store that value
            // here

            // Helper function to print info messages to the chat window
            function print(infoMessage, asHtml) {
                var $msg = $('<div class="info">');
                if (asHtml) {
                    $msg.html(infoMessage);
                } else {
                    $msg.text(infoMessage);
                }
                $chatWindow.append($msg);
            }

            // Helper function to print chat message to the chat window
            function printMessage(fromUser, message) {
                var $user = $('<span class="username">').text(displayName(fromUser) + ':');
                if (fromUser === username) {
                    $user.addClass('me');
                }
                var $message = $('<span class="message">').text(message);
                var $container = $('<div class="message-container">');
                $container.append($user).append($message);
                $chatWindow.append($container);
                $chatWindow.scrollTop($chatWindow[0].scrollHeight);
            }


            // Get an access token for the current user, passing a username (identity)
            // and a device ID - for browser-based apps, we'll always just use the
            // value "browser"


            function startChat() {
                // Initialize the Chat client
                new Twilio.Chat.Client.create(accessToken).then(function (test) {
                    chatClient = test;
                    chatClient.getSubscribedChannels().then(createOrJoinGeneralChannel);

                });


            }

            function createOrJoinGeneralChannel() {
                // Get the general chat channel, which is where all the messages are
                // sent in this simple application
                console.log('create chatroom');
                print('Attempting to join "general" chat channel...');
                var promise = chatClient.getChannelByUniqueName(lessonName);
                promise.then(function (channel) {
                    generalChannel = channel;
                    console.log('Found channel:');
                    console.log(generalChannel);
                    setupChannel();
                }).catch(function () {
                    // If it doesn't exist, let's create it
                    console.log('Creating channel');
                    chatClient.createChannel({
                        uniqueName: lessonName,
                        friendlyName: lessonName + ' Chat Channel'
                    }).then(function (channel) {
                        console.log('Created channel:');
                        console.log(channel);
                        generalChannel = channel;
                        setupChannel();
                    });
                });
            }

            function displayName(name) {
                var tmp = name.split('_');
                return tmp[1];
            }

            // Set up channel after it has been found
            function setupChannel() {
                // Join the general channel
                generalChannel.join().then(function (channel) {
                    print('Joined channel as '
                        + '<span class="me">' + username + '</span>.', true);
                });

                // Listen for new messages sent to the channel
                generalChannel.on('messageAdded', function (message) {
                    printMessage(message.author, message.body);
                });
            }

            // Send a new message to the general channel
            var $input = $('#chat-input');
            $input.on('keydown', function (e) {
                if (e.keyCode == 13) {
                    console.log(generalChannel);
                    generalChannel.sendMessage($input.val())
                    $input.val('');
                }
            });
        });
    </script>
@endsection