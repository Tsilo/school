@extends('layouts.app')

@section('content')
    <section class="page-wrap">
        <div class="container">
            <div class="col-md-8 col-md-offset-2">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>სახელი</th>
                        <th>დრო</th>
                        <th><span class="glyphicon glyphicon-play-circle" aria-hidden="true"></span></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($lessons as $lesson)
                        <tr>
                            <td>{{ $lesson->id }}</td>
                            <td>{{ $lesson->name }}</td>
                            <td>{{ $lesson->start_date->format('h:i d/m/y') }}</td>
                            <td>
                                <a href="{{url('teacher/start', $lesson->id)}}" class="btn btn-success btn-xs">
                                    ოთახი <span class="glyphicon glyphicon-play-circle" aria-hidden="true"></span></a>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </section>


@endsection
