@extends('layouts.app')

@section('content')
    <!-- Content Start -->
    <section class="page-wrap">
        <div class="container">

            <div class="steps">
                <ul class="step-list">
                    <li class="active">
                        <div class="number"></div>
                        <div class="text">რეგისტრაცია</div>
                    </li>
                    <li>
                        <div class="number"></div>
                        <div class="text">ცხრილი</div>
                    </li>
                    <li>
                        <div class="number"></div>
                        <div class="text">გადახდა</div>
                    </li>
                </ul>
            </div>

            <div class="registration-content">
                <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}
                    <div class="form-row {{ $errors->has('name') ? ' has-error' : '' }}">
                        <input id="firstname" type="text" class="form-input" name="name"
                               value="{{ old('name') }}" required autofocus>
                        <label for="firstname" class="form-label">საელი</label>
                        @if ($errors->has('name'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('lastname') ? ' has-error' : '' }}">
                        <input id="lastname" type="text" class="form-input" name="lastname"
                               value="{{ old('lastname') }}" required>
                        <label for="lastname" class="form-label">გვარი</label>
                        @if ($errors->has('lastname'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('id_number') ? ' has-error' : '' }}">
                        <input id="id_number" type="text" class="form-input" name="id_number"
                               value="{{ old('id_number') }}" required>
                        <label for="id_number" class="form-label">პირადი ნომერი</label>
                        @if ($errors->has('id_number'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('id_number') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input id="email" type="text" class="form-input" name="email"
                               value="{{ old('email') }}" required>
                        <label for="email" class="form-label">Email</label>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('password') ? ' has-error' : '' }}">
                        <input id="password" type="password" class="form-input" name="password" required>
                        <label for="password" class="form-label">პაროლი</label>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-row @if ($errors->has('birthday') || $errors->has('birthmonth') || $errors->has('birthyear')) has-error @endif">
                        <div class="date-note">რეგისტრაციისთვის საჭიროა იყოთ 16 წელს ზევით</div>
                        <div class="form-select">
                            <select class="day" name="birthday">
                                <option value="0" disabled selected>დღე</option>
                                @for ($i = 1; $i < 32; $i++)
                                    <option value="{{ $i }}" {{ (old('birthday') == $i ? "selected":"") }}>{{ $i }}</option>
                                @endfor
                            </select>
                            <select class="month" name="birthmonth">
                                <option value="0" disabled selected>თვე</option>
                                @php
                                    $months =  config('global.shortMonths');
                                @endphp
                                @for ($i = 1; $i < 13; $i++)
                                    <option value="{{ $i }}" {{ (old('birthmonth') == $i ? "selected":"") }}>
                                        {{ $months[$i] }}</option>
                                @endfor
                            </select>
                            <select class="year" name="birthyear">
                                <option value="0" disabled selected>წელი</option>
                                <?php $year = date("Y") - 16; ?>
                                @for ($i = $year; $i > $year - 51; $i--)
                                    <option value="{{ $i }}" {{ (old('birthyear') == $i ? "selected":"") }}>{{ $i }}</option>
                                @endfor
                            </select>
                        </div>
                        @if ($errors->has('birthday') || $errors->has('birthmonth') || $errors->has('birthyear'))
                            <span class="help-block">
                                        <strong>აირჩიეთ დაბადების თარიღი</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-row{{ $errors->has('mobilenumber') ? ' has-error' : '' }}">
                        <input id="mobile-number-reg" type="text" class="form-input" name="mobilenumber"
                               value="{{ old('mobilenumber') }}" required>
                        <label for="mobile-number-reg" class="form-label">მობილური ტელეფონი</label>
                        @if ($errors->has('mobilenumber'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('mobilenumber') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-row clear-fix">
                        <input id="sms-code-reg" type="text" class="form-input with-btn f-left">
                        <label for="sms-code-reg" class="form-label">SMS კოდი</label>
                        <button class="form-btn small f-right disabled">SMS</button>
                    </div>
                    <div class="form-row">
                        <div class="terms-conditions">
                            ღილაკზე "რეგისტრაცია" დაჭერით თქვენ <br>
                            ეთანხმებით <a href="#">მოხმარების წესებს და პირობებს</a>
                        </div>
                    </div>
                    <div class="form-row">
                        <button class="form-btn" type="submit">რეგისტრაცია</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- Content End -->

@endsection
