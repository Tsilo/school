@extends('layouts.app')
@section('content')
    <!-- Header Start -->
    <section class="main-header">
        <div class="container">
            <div class="main-content">
                <h1>მართვის მოწმობის ონლაინ კურსი</h1>
                <h5>გაიარე მართვის მოწმობის კურსი შენთვის სასურველი ადგილიდან <br> და დაუსვი მასწავლებელს კითხვები ლაივ
                    რეჟიმში</h5>
                <a href="{{ route('register') }}" class="button large white blue">დაიწყე დღესვე</a>
            </div>

            <div class="scroll-bottom">
                <div class="title">გაიგე მეტი</div>
                <a href="#" class="icon-arrow-down"></a>
            </div>
        </div>
    </section>
    <!-- Header End -->

    <!-- Features Start -->
    <section class="features">
        <div class="container">
            <h1>რატომ ჩვენთან?!</h1>
            <h5>ჩვენი ლექციები განსაკუთრებულია იმით, რომ ლექცია მიმდინარეობს ლაივ რეჟიმში, <br> არანაირი ვიდეო ჩანაწერი,
                შენ შეგიძლია დაუსვა მასწავლებელს კითხვა როცა მეგესურვება.</h5>
            <div class="row">
                <div class="features-item col-md-4">
                    <div class="features-icon piggy"></div>
                    <div class="title">დაბალი ფასი</div>
                    <div class="description">ჩვენთან კურსი <br> უფრო იაფია ვიდრე სხვაგან</div>
                </div>
                <div class="features-item col-md-4">
                    <div class="features-icon sofa"></div>
                    <div class="title">კომფორტი</div>
                    <div class="description">გარემოს, დროს და <br> ადგილს ირჩევთ თქვენ</div>
                </div>
                <div class="features-item col-md-4">
                    <div class="features-icon quality-badge"></div>
                    <div class="title">ხარისხი</div>
                    <div class="description">ჩვენი მასწავლებლები და <br> სასწავლო პროგრამა საუკეთესოა</div>
                </div>
            </div>
        </div>
    </section>
    <!-- Features End -->


    <!-- How Works Start -->
    <section class="how-works">
        <div class="container">
            <h1>როგორ მუშაობს</h1>
            <div class="row">
                <div class="work-item col-md-4">
                    <div class="icon-cont">
                        <div class="work-icon register"></div>
                    </div>
                    <div class="title">1. დარეგისტრირდი</div>
                    <div class="description">გადადი რეგისტრაციის ბმულზე <br> და გაიარე რეგისტრაცია მარტივად</div>
                </div>
                <div class="work-item col-md-4">
                    <div class="icon-cont">
                        <div class="work-icon calendar"></div>
                    </div>
                    <div class="title">2. შეადგინე განრიგი</div>
                    <div class="description">აირჩიე შენთვის სასურველი <br> დასწრების დღეები და საათები</div>
                </div>
                <!-- <div class="work-item col-md-3">
                    <div class="icon-cont">
                        <div class="num">3</div>
                        <div class="icon icon-ccard"></div>
                    </div>
                    <div class="title">გადაიხადე</div>
                    <div class="description">გადაიხადე თანხა მარტივად <br> ბარათით ან ნებისმიერი ბანკიდან</div>
                </div> -->
                <div class="work-item col-md-4">
                    <div class="icon-cont">
                        <div class="work-icon lecture"></div>
                    </div>
                    <div class="title">3. დაესწარი ლექციას</div>
                    <div class="description">დაესწარი ლექციას შენთვის <br> სასურველი ადგილიდან</div>
                </div>
            </div>
            <!-- <div class="line-title"><span>არ გადადო</span></div> -->
            <a href="{{ route('register') }}" class="start-now button large white">დაიწყე დღესვე</a>
        </div>
    </section>
    <!-- How Works End -->



@endsection

