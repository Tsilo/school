require('./bootstrap');
require('bootstrap-datepicker');


$('.datepicker').datepicker({
    autoclose: true,
    startDate: new Date(),
    format: "dd/mm/yy",
    todayHighlight: true

});

$('.deleteRow').click(function () {
    if (confirm('ნამდვილად წავშალო?')) {
        var row = $(this);
        axios({
            method: 'delete',
            url: row.data('url')
        }).then(function (response) {

            $('#infoModalTitle').html('Delete');
            $('#infoModalBody').html(response.data.message);
            $('#infoModal').modal('show');
            row.closest('tr').remove();
        }).catch(function (error) {

            $('#infoModalTitle').html('Delete');
            $('#infoModalBody').html(error.data.error);
            $('#infoModal').modal('show');
        });
    }
});