/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Vue.component('example', require('./components/Example.vue'));

// const app = new Vue({
//     el: '#app'
// });


$(function () {

    /* Scroll On Arrow Click*/
    $(".scroll-bottom").click(function () {
        $('html, body').animate({
            scrollTop: $(".features").offset().top
        }, 1000);
    });

    /* Input Check if not empty*/
    $('.form-input').blur(function () {
        if ($(this).val().length === 0) {
            $(this).removeClass('focused');
        } else {
            $(this).addClass('focused');
        }
    });

    $('.form-input').each(
        function () {
            var val = $(this).val().trim();
            if (val == '') {
                $(this).removeClass('focused');
            } else {
                $(this).addClass('focused');
            }
        }
    );

    /* Login Popup Show/Hide*/
    $("#login-btn").click(function () {
        $(".popup").removeClass("hidden");
    });

    $('.close-box').click(function () {
        $(".popup").addClass("hidden");
        $(".popup .login").removeClass("hidden");
        $(".popup .recovery").addClass("hidden");
    });

    $(".popup").on('blur', function () {
        $(this).addClass("hidden");
    });

    $('.popup .login .form-btn').click(function (e) {
        e.preventDefault();
        $('.popup .login .help-block').addClass('hidden');
        axios({
            method: 'post',
            url: '/ajaxlogin',
            data: {
                'login': $('#mobile-number').val(),
                'password': $('#login-password').val()
            }
        }).then(function (response) {
            var data = response.data;
            $('.popup .login .form-row:first-child').removeClass('has-error');
            window.location.href = data.redirect;

        }).catch(function (error) {
            $('.popup .login .form-row:first-child').addClass('has-error');
            $('.popup .login .help-block').removeClass('hidden');
            console.log(error.response.data);
        });
    });

    /* Forgot Password Show */
    $(".forgot-pass").click(function () {
        $(".popup .login").addClass("hidden");
        $(".popup .recovery").removeClass("hidden");
    });

    /* FAQ Collapse */
    $(".faq-item .faq-header").click(function () {
        if($(this).parent().hasClass("active")){
            $(this).parent().removeClass("active");
        }else {
            $(".faq-item").removeClass("active");
            $(this).parent().addClass("active");
        }
    });
});


/* Calendar ToolTip */
var calendar = {
    el: {},
    data: {
        calendarStep: 1,
        calendarPagination: 0
    },
    init: function () {

        //get data for graph and drow
        this.getEvents();

    }
    ,
    getEvents: function () {
        //hide all popups
        $('.calendar-container .calendar-body .cal-col').on('click', function () {
            if ($(".days-tooltip").hasClass("show")) {
                $(".calendar-container .calendar-body .cal-col .days-tooltip").removeClass("show");
            }
        });
        /* ToolTip Show */
        $(".calendar-container .calendar-body").on('click', '.available', function () {
            $(this).find(".days-tooltip").addClass("show");
        });
        $('.days-tooltip').click(function (event) {
            event.stopPropagation();
        });
        /* Save */

        $(".save").click(function () {
            calendar.method.selectHour($(this).closest(".cal-col"));
        });
        /* Ok Button */
        $(".ok-btn").click(function () {
            $(this).parent().parent().parent().addClass("hidden")
        });
        /*month switcher*/
        $('.month-switcher .left-arrow').click(function () {
            calendar.method.paginateMonth('prev')
        });
        $('.month-switcher .right-arrow').click(function () {
            calendar.method.paginateMonth('next')
        });
    },
    method: {
        selectHour: function (td) {
            //disable all active days
            $('.calendar-container .calendar-body .cal-col.available').removeClass("available").addClass("disabled");
            //show selected hour
            td.find('.days-tooltip').removeClass("show");
            td.find(".time").html(td.find('select option:selected').text());
            td.find(".lesson").removeClass("hidden");

            if (calendar.data.calendarStep < 10) {
                //move to next step
                $('#lesson' + calendar.data.calendarStep).val(td.find('select').val());
                calendar.data.calendarStep += 1;
                calendar.method.nextLessonsData(td.data('date'));
            }else if(calendar.data.calendarStep == 10) {
                $('#lesson' + calendar.data.calendarStep).val(td.find('select').val());
                $('#calendarSubmit').removeClass('hidden');
            }
        },
        nextLessonsData: function (date) {
            axios({
                method: 'post',
                url: '/calendar/nextTwoDays',
                data: {'date': date, 'lesson': calendar.data.calendarStep}
            }).then(function (response) {
                var data = response.data;
                $.each(response.data, function (key, val) {
                    el = $('.calendar-container .cal-col[data-date="' + val.date + '"]');
                    if (el.length) {
                        //if next lesson is hidden go to next month
                        if (el.is(":hidden")) {
                            calendar.method.paginateMonth('next')
                        }
                        el.removeClass('disabled').addClass('available');
                        el.find('.lesson .number span').html(calendar.data.calendarStep);
                        el.find('.days-tooltip  .form-select select').append('<option value="' + val.id + '">' + val.hour + '</option>')
                    } else {
                        alert('error has accrued please contact adminsitrator');
                    }
                });

            }).catch(function (error) {
                console.log(error.response.data);
            });
        },
        paginateMonth: function (nav) {
            $('#calendarMonth' + calendar.data.calendarPagination).addClass('hidden');
            if (nav == 'next') {
                calendar.data.calendarPagination++;
            } else {
                calendar.data.calendarPagination--;
            }
            $('#calendarMonth' + calendar.data.calendarPagination).removeClass('hidden');
        }
    }
};
calendar.init();


