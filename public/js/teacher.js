(function () {
    Twilio.Video.createLocalTracks({
        audio: true,
        video: {width: 300}
    }).then(function (localTracks) {
        return Twilio.Video.connect('{{ $accessToken }}', {
            name: 'test',
            tracks: localTracks,
            video: {width: 300}
        });
    }).then(function (room) {
        console.log('Successfully joined a Room: ', room.name);

        room.participants.forEach(participantConnected);

        var previewContainer = document.getElementById(room.localParticipant.sid);
        if (!previewContainer || !previewContainer.querySelector('video')) {
            participantConnected(room.localParticipant);
        }

        room.on('participantConnected', function (participant) {
            console.log("Joining: '" + participant.identity + "'");
            participantConnected(participant);
        });

        room.on('participantDisconnected', function (participant) {
            console.log("Disconnected: '" + participant.identity + "'");
            participantDisconnected(participant);
        });
    });

    function participantConnected(participant) {
        console.log('Participant "%s" connected', participant.identity);

        const div = document.createElement('div');
        div.id = participant.sid;
        div.setAttribute("style", "float: left; margin: 10px;");
        div.innerHTML = "<div style='clear:both'>" + participant.identity + "</div>";

        participant.tracks.forEach(function(track) {
            trackAdded(div, track)
        });

        participant.on('trackAdded', function(track) {
            trackAdded(div, track)
        });
        participant.on('trackRemoved', trackRemoved);

        document.getElementById('media-div').appendChild(div);
    }

    function participantDisconnected(participant) {
        console.log('Participant "%s" disconnected', participant.identity);

        participant.tracks.forEach(trackRemoved);
        document.getElementById(participant.sid).remove();
    }

    function participantConnected(participant) {
        console.log('Participant "%s" connected', participant.identity);

        const div = document.createElement('div');
        div.id = participant.sid;
        div.setAttribute("style", "float: left; margin: 10px;");
        div.innerHTML = "<div style='clear:both'>" + participant.identity + "</div>";

        participant.tracks.forEach(function(track) {
            trackAdded(div, track)
        });

        participant.on('trackAdded', function(track) {
            trackAdded(div, track)
        });
        participant.on('trackRemoved', trackRemoved);

        document.getElementById('media-div').appendChild(div);
    }

    function participantDisconnected(participant) {
        console.log('Participant "%s" disconnected', participant.identity);

        participant.tracks.forEach(trackRemoved);
        document.getElementById(participant.sid).remove();
    }

    function trackAdded(div, track) {
        div.appendChild(track.attach());
        var video = div.getElementsByTagName("video")[0];
        if (video) {
            video.setAttribute("style", "max-width:300px;");
        }
    }

    function trackRemoved(track) {
        track.detach().forEach( function(element) { element.remove() });
    }

});
/*
/!**
 * Created by zura on 3/9/17.
 *!/
var server = null;
if (window.location.protocol === 'http:')
    server = "http://" + window.location.hostname + ":8088/janus";
else
    server = "https://" + window.location.hostname + ":8089/janus";

var janus = null;
var started = false;
var roomhandler = null;
var opaqueId = 'techer1';
var roomId = 12;
var mystream = null;
var myusername = 'zura';
var feeds = [];
var bitrateTimer = [];

// Initialize the library (all console debuggers enabled)
Janus.init({
    debug: "all", callback: function () {
        $('#start').click(function () {
            if (started)
                return;
            started = true;
            $(this).attr('disabled', true).unbind('click');
            // Make sure the browser supports WebRTC
            if (!Janus.isWebrtcSupported()) {
                bootbox.alert("No WebRTC support... ");
                return;
            }

            // Create session
            janus = new Janus(
                {
                    server: server,
                    success: function () {

                        // Attach to video room test plugin
                        janus.attach(
                            {
                                plugin: "janus.plugin.videoroom",
                                opaqueId: opaqueId,
                                success: function (pluginHandle) {
                                    roomhandler = pluginHandle;
                                    Janus.log("Plugin attached! (" + roomhandler.getPlugin() + ", id=" + roomhandler.getId() + ")");
                                    Janus.log("  -- This is a publisher/manager");

                                    var createRoom = {"request": "create", "room": roomId, "description": "Lesson"};
                                    roomhandler.send({"message": createRoom});

                                    // Prepare the username registration
                                    var register = {
                                        "request": "join",
                                        "room": roomId,
                                        "ptype": "publisher",
                                        "display": myusername
                                    };
                                    roomhandler.send({"message": register});

                                },
                                error: function (error) {
                                    Janus.error("  -- Error attaching plugin...", error);
                                    bootbox.alert("Error attaching plugin... " + error);
                                },
                                consentDialog: function (on) {
                                    Janus.debug("Consent dialog should be " + (on ? "on" : "off") + " now");
                                    if (on) {
                                        // Darken screen and show hint
                                        $.blockUI({
                                            message: '<div><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span></div>',
                                            css: {
                                                border: 'none',
                                                padding: '15px',
                                                backgroundColor: 'transparent',
                                                color: '#aaa',
                                                top: '10px',
                                                left: (navigator.mozGetUserMedia ? '-100px' : '300px')
                                            }
                                        });
                                    } else {
                                        // Restore screen
                                        $.unblockUI();
                                    }
                                },
                                mediaState: function (medium, on) {
                                    Janus.log("Janus " + (on ? "started" : "stopped") + " receiving our " + medium);
                                },
                                webrtcState: function (on) {
                                    Janus.log("Janus says our WebRTC PeerConnection is " + (on ? "up" : "down") + " now");
                                    $("#videolocal").parent().parent().unblock();
                                },
                                onmessage: function (msg, jsep) {
                                    console.log(msg, jsep);
                                    Janus.debug(" ::: Got a message (publisher) :::");
                                    Janus.debug(JSON.stringify(msg));
                                    var event = msg["videoroom"];
                                    Janus.debug("Event: " + event);
                                    if (event != undefined && event != null) {
                                        if (event === "joined") {
                                            // Publisher/manager created, negotiate WebRTC and attach to existing feeds, if any
                                            myid = msg["id"];
                                            mypvtid = msg["private_id"];
                                            Janus.log("Successfully joined room " + msg["room"] + " with ID " + myid);
                                            publishOwnFeed(true);
                                        } else if (event === "destroyed") {
                                            // The room has been destroyed
                                            Janus.warn("The room has been destroyed!");
                                            bootbox.alert("The room has been destroyed", function () {
                                                window.location.reload();
                                            });
                                        } else if (event === "event") {
                                            // Any new feed to attach to?
                                            if (msg["error"] !== undefined && msg["error"] !== null) {
                                                bootbox.alert(msg["error"]);
                                            }
                                        }
                                    }
                                    if (jsep !== undefined && jsep !== null) {
                                        Janus.debug("Handling SDP as well...");
                                        Janus.debug(jsep);
                                        roomhandler.handleRemoteJsep({jsep: jsep});
                                    }
                                },
                                onlocalstream: function (stream) {
                                    Janus.debug(" ::: Got a local stream :::");
                                    mystream = stream;
                                    Janus.debug(JSON.stringify(stream));
                                    $('#videolocal').empty();
                                    $('#videojoin').hide();
                                    $('#videos').removeClass('hide').show();
                                    if ($('#myvideo').length === 0) {
                                        $('#videolocal').append('<video class="rounded centered" id="myvideo" width="100%" height="100%" autoplay muted="muted"/>');
                                        // Add a 'mute' button
                                        $('#videolocal').append('<button class="btn btn-warning btn-xs" id="mute" style="position: absolute; bottom: 0px; left: 0px; margin: 15px;">Mute</button>');
                                        $('#mute').click(toggleMute);
                                        // Add an 'unpublish' button
                                        $('#videolocal').append('<button class="btn btn-warning btn-xs" id="unpublish" style="position: absolute; bottom: 0px; right: 0px; margin: 15px;">Unpublish</button>');
                                        $('#unpublish').click(unpublishOwnFeed);
                                    }
                                    $('#publisher').removeClass('hide').html(myusername).show();
                                    Janus.attachMediaStream($('#myvideo').get(0), stream);
                                    $("#myvideo").get(0).muted = "muted";
                                    $("#videolocal").parent().parent().block({
                                        message: '<b>Publishing...</b>',
                                        css: {
                                            border: 'none',
                                            backgroundColor: 'transparent',
                                            color: 'white'
                                        }
                                    });
                                    var videoTracks = stream.getVideoTracks();
                                    if (videoTracks === null || videoTracks === undefined || videoTracks.length === 0) {
                                        // No webcam
                                        $('#myvideo').hide();
                                        $('#videolocal').append(
                                            '<div class="no-video-container">' +
                                            '<i class="fa fa-video-camera fa-5 no-video-icon" style="height: 100%;"></i>' +
                                            '<span class="no-video-text" style="font-size: 16px;">No webcam available</span>' +
                                            '</div>');
                                    }
                                },
                                onremotestream: function (stream) {
                                    // The publisher stream is sendonly, we don't expect anything here
                                },
                                oncleanup: function () {
                                    Janus.log(" ::: Got a cleanup notification: we are unpublished now :::");
                                    mystream = null;
                                    $('#videolocal').html('<button id="publish" class="btn btn-primary">Publish</button>');
                                    $('#publish').click(function () {
                                        publishOwnFeed(true);
                                    });
                                    $("#videolocal").parent().parent().unblock();
                                }
                            });
                    },
                    error: function (error) {
                        Janus.error(error);
                        bootbox.alert(error, function () {
                            window.location.reload();
                        });
                    },
                    destroyed: function () {
                        window.location.reload();
                    }
                });
            console.log(janus);
        });
    }
});


function publishOwnFeed(useAudio) {
    // Publish our stream
    $('#publish').attr('disabled', true).unbind('click');
    roomhandler.createOffer(
        {
            // Add data:true here if you want to publish datachannels as well
            media: {
                audioRecv: false,
                videoRecv: false,
                audioSend: useAudio,
                videoSend: true,
                video: 'lowres'
            },	// Publishers are sendonly
            success: function (jsep) {
                Janus.debug("Got publisher SDP!");
                Janus.debug(jsep);
                var publish = {"request": "configure", "audio": useAudio, "video": true};
                roomhandler.send({"message": publish, "jsep": jsep});
            },
            error: function (error) {
                Janus.error("WebRTC error:", error);
                if (useAudio) {
                    publishOwnFeed(false);
                } else {
                    bootbox.alert("WebRTC error... " + JSON.stringify(error));
                    $('#publish').removeAttr('disabled').click(function () {
                        publishOwnFeed(true);
                    });
                }
            }
        });
}
function toggleMute() {
    var muted = roomhandler.isAudioMuted();
    Janus.log((muted ? "Unmuting" : "Muting") + " local stream...");
    if (muted)
        roomhandler.unmuteAudio();
    else
        roomhandler.muteAudio();
    muted = roomhandler.isAudioMuted();
    $('#mute').html(muted ? "Unmute" : "Mute");
}
function unpublishOwnFeed() {
    // Unpublish our stream
    $('#unpublish').attr('disabled', true).unbind('click');
    var unpublish = {"request": "unpublish"};
    roomhandler.send({"message": unpublish});
}*/
