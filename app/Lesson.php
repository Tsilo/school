<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'name', 'teacher_id', 'course_id', 'start_date', 'lesson', 'status'
    ];

    protected $dates = [
        'start_date'
    ];

    public function course()
    {
        return $this->belongsTo('App\Course');
    }

    public function teacher()
    {
        return $this->belongsTo('App\User', 'teacher_id');
    }

    public function user(){
        return $this->belongsToMany('App\User');
    }
}
