<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'lastname', 'birthdate', 'mobilenumber', 'email', 'password', 'id_number', 'type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function lessons()
    {
        return $this->belongsToMany('App\Lesson')->withPivot('status');
    }

    public function teacherLessons(){
     return $this->hasMany('App\Lesson', 'teacher_id');
    }

    public function UserLessons(){
        return $this->belongsToMany('App\Lesson');
    }
}
