<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name', 'teacher_id', 'start_date'
    ];

    public function lessons()
    {
        return $this->hasMany('App\Lesson');
    }
}
