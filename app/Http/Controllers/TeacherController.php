<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lesson;
use Illuminate\Support\Facades\Auth;
use Twilio\Rest\Client;

class TeacherController extends Controller
{
    public function index(){
        $lessons = Lesson::all()->where('teacher_id', Auth::user()->id);
        return view('teacher.index', ['lessons' => $lessons]);
    }
    public function startLesson($lid)
    {
        $name = ucfirst(Auth::user()->name) .'.'. ucfirst(substr(Auth::user()->lastname, 0 , 1 ));
        return view('teacher/lesson', ['lessonName' => 'lesson_'.$lid, 'lid' => $lid, 'chatName' => $name]);
    }
}
