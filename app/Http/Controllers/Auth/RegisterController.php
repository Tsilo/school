<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/calendar';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $year = date("Y") - 16;
        return Validator::make($data, [
            'name' => 'required|max:255',
            'lastname' => 'required|max:255',
            'birthday' => 'required|integer|max:31',
            'birthmonth' => 'required|integer|max:12',
            'birthyear' => 'required|integer|max:' . $year,
            'email' => 'required|email|max:255|unique:users',
            'id_number' => 'required|min:11|max:11',
            'password' => 'required|min:6',
            'mobilenumber' => 'required|numeric|min:9|unique:users',
        ], [
            'name.required' => 'შეიყვანეთ სახელი',
            'lastname' => 'შეიყვანეთ გვარი',
            'birthday' => 'აირჩიეთ დაბადების თარიღი',
            'birthmonth' => 'აირჩიეთ დაბადების თარიღი',
            'birthyear' => 'აირჩიეთ დაბადების თარიღი',
            'email.unique' => 'ასეთი მომხმარებელი უკვე არსებობს',
            'email.email' => 'შეიყვანეთ სწორი მისამართი',
            'id_number.*' => 'შეიყვანეთ პირადი ნომერი',
            'password.min' => 'პაროლი მინიმუმ 6 სიბოლისგან უნდა შედგებოდეს',
            'password.confirmed' => 'განმეორებითი პაროლი არ ემთხვევა',
            'mobilenumber.unique' => 'ასეთი ნომერი უკვე რეგისტრირებულია',
            'mobilenumber.*' => 'შეიყვანეთ მობილურის ნომერი',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'lastname' => $data['lastname'],
            'birthdate' => $data['birthyear'].'-'.$data['birthmonth'].'-'.$data['birthday'],
            'mobilenumber' => $data['mobilenumber'],
            'email' => $data['email'],
            'id_number' => $data['id_number'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
