<?php

namespace App\Http\Controllers;

use App\Course;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::paginate(15);
        return view('admin.course.courses', ['courses' => $courses]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $teachers = User::where('type', 2)->get();
        return view('admin.course.create', ['teachers' => $teachers]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $date = Carbon::createFromFormat('d/m/y G:i', $request['start_date'] . ' ' . $request['start_hour']);

        $course = Course::create([
            "name" => $request['name'],
            "teacher_id" => $request['teacher_id'],
            "start_date" => $date
        ]);

        if ($course) {
            return redirect()->route('lesson.create',
                [
                    'teacher_id' => $request['teacher_id'],
                    'start_date' => $request['start_date'],
                    'start_hour' => $request['start_hour'],
                    'course_id' => $course->id
                ]);
        } else {
            return redirect()->back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $course = Course::find($id);
        $teachers = User::where('type', 2)->get();

        return view('admin.course.edit', ['course' => $course, 'teachers' => $teachers]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $course = Course::findOrFail($id);

        $input = $request->all();

        $course->fill($input)->save();

        $request->session()->flash('flash_message', 'კურსი წარმატებით განახლდა');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {

            $course = Course::findOrFail($id);

            if ($course->delete()) {
                return response()->json(['message' => 'sucessfully deleted']);
            } else {
                return response()->json(['error' => 'failed delete'], 500);
            }

        }
    }
}
