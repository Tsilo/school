<?php

namespace App\Http\Controllers;

use App\Lesson;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function login(Request $request)
    {
        if ($request->ajax()) {
            $auth = Auth::attempt(['mobilenumber' => $request['login'], 'password' => $request['password']]);
            if (!$auth) {
                $auth = Auth::attempt(['email' => $request['login'], 'password' => $request['password']]);
            }
            if ($auth) {
                $redirectTo = 'dashboard';
                if (Auth::user()->type == 2) {
                    $redirectTo = '/teacher';
                } elseif (Auth::user()->type == 1) {
                    $redirectTo = '/admin';
                }
                return response()->json(['redirect' => $redirectTo]);
            } else {
                return response()->json(['error' => 'ასეთი მომხმარებელი/პაროლი არ არსებობს'], 422);
            }
        }
        return false;
    }

    public function userLessons(Request $request)
    {
        $today = Carbon::now();
        $schedule = $request->user()->lessons;

        return view('userLessons', ['today' => $today, 'schedule' => $schedule]);
    }

    public function userDashboard()
    {

        $lessons = User::find(Auth::user()->id)->UserLessons()->with('teacher')->withPivot('status')->get();
        if(empty($lessons)){
            redirect('/calendar');
        }
        return view('student.dashboard', ['lessons' => $lessons]);
    }

    public function joinLesson($id)
    {

        return view('student/lesson', ['lessonName' => 'lesson_' . $id]);
    }

    public function payment()
    {
        return view('payment');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = user::paginate(15);
        return view('admin.user.users', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user = User::create([
            'name' => $request['name'],
            'lastname' => $request['lastname'],
            'birthdate' => $request['birthyear'] . '-' . $request['birthmonth'] . '-' . $request['birthday'],
            'mobilenumber' => $request['mobilenumber'],
            'type' => $request['type'],
            'email' => $request['email'],
            'id_number' => $request['id_number'],
            'password' => bcrypt($request['password']),
        ]);

        if ($user) {
            $request->session()->flash('flash_message', 'მომხმარებელი წარმატებით დაემატა');

            return redirect()->route('user.index');
        } else {
            return redirect()->back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('admin.user.edit', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $input = $request->all();

        $tmp = $user->fill($input)->save();
        //dd($tmp, $input);
        if ($tmp) {
            $request->session()->flash('flash_message', 'მომხმარებელი წარმატებით განახლდა');
        } else {
            $request->session()->flash('flash_message', 'error');
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {

            $user = User::findOrFail($id);

            if ($user->delete()) {
                return response()->json(['message' => 'sucessfully deleted']);
            } else {
                return response()->json(['error' => 'failed delete'], 500);
            }

        }
    }
}
