<?php namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Twilio\Rest\Client;
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\VideoGrant;
use Twilio\Jwt\Grants\ChatGrant;

class TwilioController extends Controller
{
    protected $sid;
    protected $token;
    protected $key;
    protected $secret;
    protected $csid;

    public function __construct()
    {
        $this->sid = config('services.twilio.acc_sid');
        $this->token = config('services.twilio.acc_token');
        $this->vsid = config('services.twilio.video_sid');
        $this->secret = config('services.twilio.video_secret');
        $this->csid = config('services.twilio.chat_sid');


        //$this->token ='eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImN0eSI6InR3aWxpby1mcGE7dj0xIn0.eyJqdGkiOiJTSzZlZjA3Y2JiMzZlNWQ2MThmYjg4OTBiMTdkZTdiYjAxLTE1MTgzNDE5ODQiLCJpc3MiOiJTSzZlZjA3Y2JiMzZlNWQ2MThmYjg4OTBiMTdkZTdiYjAxIiwic3ViIjoiQUMyMzM0NDBlNzZmMWU4MTVkMzNjMzUwMjRlMzQwOWU4MiIsImV4cCI6MTUxODM0NTU4NCwiZ3JhbnRzIjp7ImlkZW50aXR5IjoiYWRtaW4iLCJ2aWRlbyI6eyJyb29tIjoidGVzdCJ9fX0.pj4cYTD5fv5UZLz0iz_JRDBKAedctT45QlWLCI8d2vc';
    }

    public function generateToken()
    {

    }

    public function showRooms()
    {
        $rooms = [];
        try {
            $client = new Client($this->sid, $this->token);
            $allRooms = $client->video->rooms->read([]);

            $rooms = array_map(function ($room) {
                return $room->uniqueName;
            }, $allRooms);

        } catch (Exception $e) {
            echo "Error: " . $e->getMessage();
        }
        return view('index', ['rooms' => $rooms]);
    }

    public function checkRoom(Request $request){
        $client = new Client($this->sid, $this->token);
        $exists = $client->video->rooms->read(['uniqueName' => $request->roomName]);
        if (empty($exists)) {
            return response()->json(['status' => false]);
        }else{
            return response()->json(['status' => true]);

        }
    }

    public function createRoom(Request $request)
    {
        $client = new Client($this->sid, $this->token);

        $exists = $client->video->rooms->read(['uniqueName' => $request->roomName]);

        if (empty($exists)) {
            $client->video->rooms->create([
                'uniqueName' => $request->roomName,
                'type' => 'group',
                'recordParticipantsOnConnect' => false
            ]);

        }

        return response()->json(['roomName' => $request->roomName]);
    }

    public function joinRoom(Request $request, VideoGrant $videoGrant, ChatGrant $chatGrant)
    {
        // A unique identifier for this user
        $identity = Auth::user()->id . '_' . ucfirst(Auth::user()->name) .'.'. ucfirst(substr(Auth::user()->lastname, 0 , 1 ));

        $token = new AccessToken($this->sid, $this->vsid, $this->secret, 3600, $identity);

        $videoGrant->setRoom($request->roomName);

        $token->addGrant($videoGrant);

        $chatGrant->setServiceSid($this->csid);
        $token->addGrant($chatGrant);

        return response()->json(['accessToken' => $token->toJWT(), 'identity' => $identity]);




    }


}
