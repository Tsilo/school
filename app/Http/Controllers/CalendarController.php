<?php

namespace App\Http\Controllers;

use App\Lesson;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CalendarController extends Controller
{
    public function index()
    {
        $shortMonths = config('global.shortMonths');
        $dt = Carbon::now();
        $today = $dt->format('Y-m-d');

        if ($dt->dayOfWeek == 0) {
            $dt->addDays(-6);
        } else {
            $dt->addDays(-($dt->dayOfWeek - 1));
        }
        for ($m = 0; $m < 5; $m++) {
            $monthTitle = $dt->day . ' ' . $shortMonths[$dt->month] . ' - ';
            $calDays[$m][] = [
                'val' => $dt->format('Y-m-d'),
                'disp' =>  ' <span class="init-day">' . $dt->day . ' <span class="day-name">' . $shortMonths[$dt->month] . '</span></span>'
            ];
            for ($i = 1; $i < 35; $i++) {
                $val = $dt->addDay()->format('Y-m-d');
                if ($dt->day == 1 || $i == 34) {
                    $disp = ' <span class="init-day">' . $dt->day . ' <span class="day-name">' . $shortMonths[$dt->month] . '</span></span>';
                } elseif ($today != $val) {
                    $disp = $dt->day;
                } else {
                    $disp = ' <span class="init-day">'  . $dt->day . ' <span class="day-name">დღეს</span></span>';
                }
                $calDays[$m][] = [
                    'val' => $val,
                    'disp' => $disp
                ];
            }

            $monthTitle .= $dt->day . ' ' . $shortMonths[$dt->month];
            $month[$m] = $monthTitle;
        }
        //get first lessons
        $lessons = Lesson::where('lesson', 1)->
        where('start_date', '>', $today)->
        where('start_date', '<', $dt->addDays(-25)->format('Y-m-d'))->
        get();
        $select = array();
        foreach ($lessons as $lesson) {
            $dt = Carbon::parse($lesson['start_date']);
            $select[$dt->format('Y-m-d')][] = ['disp' => $dt->format('H:i'), 'val' => $lesson->id];
        }

        return view('calendar', ['today' => $today, 'month' => $month, 'calDays' => $calDays, 'select' => $select]);
    }

    public function nextTwoDaysData(Request $request)
    {
        if ($request->ajax()) {

            $nextDate = Lesson::select(DB::raw('DATE(start_date) as `date`'))->
            where(DB::raw('DATE(`start_date`)'), '>', $request['date'])->
            where('lesson', $request['lesson'])->
            orderBy('start_date', 'ASC')->
            groupBy('date')->
            //offset(1)->
            limit(2)->
            get();

            if ($nextDate !== NULL) {
                if (count($nextDate) == 1) {
                    $nextDate = $nextDate[0]->date;
                } else {
                    $nextDate = $nextDate[1]->date;
                }
            } else {
                return response()->json(['error' => 'შემდეგი გაკვეთილი ვერ მოიძებნა გთხოვთ მიმართოთ ადმინისტრატორს'], 400);
            }

            $lessons = Lesson::where('lesson', $request['lesson'])->
            where(DB::raw('DATE(`start_date`)'), '>', $request['date'])->
            where(DB::raw('DATE(`start_date`)'), '<=', $nextDate)->
            orderBy('start_date', 'ASC')->get();
            $data = array();
            foreach ($lessons as $lesson) {
                $dt = Carbon::parse($lesson->start_date);
                $data[] = ['id' => $lesson->id, 'date' => $dt->format('Y-m-d'), 'hour' => $dt->format('H:i')];
            }
            if ($lessons) {
                return response()->json($data);
            } else {
                return response()->json(['error' => 'failed delete'], 400);
            }

        }

    }

    public function saveUserLessons(Request $request)
    {
        $data = $request->lesson;
        if (!empty($data)) {
            $lessons = $request->user()->lessons()->sync($request->lesson);
            if ($lessons) {
                return redirect('/payment');
            }
        }
        return redirect()->back()->withInput();
    }
}
