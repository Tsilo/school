<?php

namespace App\Http\Controllers;

use App\Lesson;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LessonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    public function getLessonsByDate(Request $request, $from, $to, $lesson = 1)
    {
        if ($request->ajax()) {

            $lessons = Lesson::all();
            if ($lessons) {
                return response()->json(['lessons' => $lessons]);
            } else {
                return response()->json(['error' => 'failed delete'], 500);
            }

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $lessonNames = [
            '0',
            '1 გაკვეთილი',
            '2 გაკვეთილი',
            '3 გაკვეთილი',
            '4 გაკვეთილი',
            '5 გაკვეთილი',
            '6 გაკვეთილი',
            '7 გაკვეთილი',
            '8 გაკვეთილი',
            '9 გაკვეთილი',
            '10 გაკვეთილი'
        ];
        $holidays = [
            '01/01',
            '01/02',
            '01/14',
            '01/19',
            '03/03',
            '03/08',
            '04/09',
            '05/09',
            '05/12',
            '05/26',
            '08/28',
            '10/14',
            '11/23',
            '12/31'
        ];
        $dt = Carbon::createFromFormat('d/m/y', $request['start_date']);
        $dates[] = $request['start_date'];
        for ($i = 0; $i < 9; $i++) {
            $dt->addDays(2);
            /*while (true) {
                if (in_array($dt->format('m/d'), $holidays) || $dt->isWeekend()) {
                    // the day is either in the holidays array or is a weekend
                    // add one day
                    $dt->addDay();
                } else {
                    break;
                }
            }*/
            $dates[] = $dt->format('d/m/y');
        }
        $teachers = User::where('type', 2)->get();
        return view('admin.lesson.create',
            ['teachers' => $teachers,
                'dates' => $dates,
                'lessonNames' => $lessonNames,
                'request' => $request->all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [];
        foreach ($request['name'] as $k => $value) {
            $data[] = ['name' => $value,
                'teacher_id' => $request['teacher_id'][$k],
                'course_id' => $request['course_id'],
                'lesson' => $k + 1,
                'start_date' => Carbon::createFromFormat('d/m/y G:i', $request['start_date'][$k] . ' ' . $request['start_hour'][$k])
            ];
        }
        //dd($data);
        $lessons = Lesson::insert($data);

        if ($lessons) {
            return redirect()->route('course.index');
        } else {
            return redirect()->back()->withInput();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        foreach ($request['id'] as $k => $rid) {
            $lesson = Lesson::findOrFail($rid);

            $input = [
                'name' => $request['name'][$k],
                'teacher_id' => $request['teacher_id'][$k],
                'start_date' => Carbon::createFromFormat('d/m/y G:i', $request['start_date'][$k] . ' ' . $request['start_hour'][$k])
            ];

            $lesson->fill($input)->save();
        }

        $request->session()->flash('flash_message', 'კურსი წარმატებით განახლდა');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function changeStatus(Request $request){
        if ($request->isMethod('post')) {
            $lesson = Lesson::findOrFail($request->id);
            $lesson->status = $request->status;
            if($lesson->save()){
                return response()->json([
                    'success' => 'true'
                ]);
            }
            return response()->json([
                'success' => 'false'
            ], 500);

        }
    }
}
