<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     * list users
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(15);
        return view('admin.user.users', ['users' => $users]);
    }

    public function createUser(){

    }

    public function destroyUser(Request $request, $id){
        if ($request->ajax()) {

            $user = User::findOrFail($id);

            if ($user->delete()) {
                return response()->json(['message' => 'sucessfully deleted']);
            } else {
                return response()->json(['error' => 'failed delete'], 500);
            }

        }
    }

}
