<?php

namespace App\Http\Middleware;

use Closure;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        $user = $request->user();
        if (!empty($user)) {
            if ($role == 'teacher' && ($user->type == 2 || $user->type == 1)) {
                return $next($request);
            } elseif ($role == 'admin' && $user->type == 1) {
                return $next($request);
            }
        }
        return abort(404);

    }
}
